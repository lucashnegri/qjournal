#include <QSqlRecord>
#include <QStringList>
#include <QTextCharFormat>
#include <QMessageBox>
#include <QInputDialog>
#include <QSqlTableModel>

#include "optionsdialog.h"
#include "noteeditordialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "contentdelegate.h"
#include "datedelegate.h"
#include "config.h"
#include "options.h"
#include "database.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), options(new Options()), editor(new NoteEditorDialog(this)),
      optionsDialog(new OptionsDialog(this)), db(new Database(this)), ui(new Ui::MainWindow())
{
    ui->setupUi(this);
    ui->categoriesView->horizontalHeader()->setStretchLastSection(true);

    ui->notesView->setItemDelegateForColumn(CATEGORY_COL,
                                            new QSqlRelationalDelegate(ui->notesView));
    ui->notesView->setItemDelegateForColumn(CONTENT_COL, new ContentDelegate(ui->notesView));
    ui->notesView->setItemDelegateForColumn(DATE_COL, new DateDelegate(ui->notesView));

    ui->searchFrom->setDate(QDate::currentDate());

    restore();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::restore()
{
    QSettings settings(QApplication::organizationName(), QApplication::applicationName());
    restoreGeometry(settings.value("windowGeometry").toByteArray());
    editor->restoreGeometry(settings.value("editorGeometry").toByteArray());
    editor->restoreState(settings.value("editorState").toByteArray());
    restoreState(settings.value("windowState").toByteArray());
    ui->splitter->restoreState(settings.value("splitterState").toByteArray());
    options->loadFromSettings(settings);
}

void MainWindow::save()
{
    QSettings settings(QApplication::organizationName(), QApplication::applicationName());
    settings.setValue("windowGeometry", saveGeometry());
    settings.setValue("windowState", saveState());
    settings.setValue("editorGeometry", editor->saveGeometry());
    settings.setValue("editorState", editor->saveState());
    settings.setValue("splitterState", ui->splitter->saveState());

    options->saveToSettings(settings);
}

void MainWindow::run()
{
    // clear the current references to the database
    ui->notesView->setModel(nullptr);
    ui->categoriesView->setModel(nullptr);
    notesModel.reset(nullptr);
    categoriesModel.reset(nullptr);
    searchModel.reset(nullptr);

    QApplication::processEvents();

    // create database connection and setup models/views
    bool res = db->loadOrCreate(options->dbPath);

    if(!res)
        QMessageBox::warning(this, tr("Database error"),
                             tr("The database could not be opened.\nConfigure the database "
                                    "path in the options dialog."));

    setupCategoriesView();

    // set the default content font
    editor->setContentFont(options->contentFont);
    ui->toolBox->setCurrentIndex(TOOLBOX_JOURNAL);

    // set window title
    setWindowTitle(tr("QJournal - %1").arg(options->dbPath));

    // show the window and compute sizes
    show();
    updateGeometry();
    QApplication::processEvents();
    dateSelected();

    // select the current date on the calendar widget
    QDate date = QDate::currentDate();
    calendarPageChanged(date.year(), date.month());
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event)
    save();
}

void MainWindow::categoryActivated(const QModelIndex &index)
{
    QSqlRecord rec = categoriesModel->record(index.row());
    int category_id = rec.value(0).toInt();
    setupNotesView(category_id);
}

void MainWindow::dateSelected()
{
    QDate date = ui->calendar->selectedDate();
    QString dateStr = date.toString(DATE_FORMAT);
    setupNotesView(dateStr);
}

void MainWindow::updateJournalInfo(QDate date)
{
    notesModel->submitAll();

    if(date.isNull())
        date = ui->calendar->selectedDate();

    // compute stats
    {
        int sTotal, sYear, sMonth;
        db->notesStatistics(date, sTotal, sYear, sMonth);

        ui->labelTotalNotes->setText(QString::number(sTotal));
        ui->labelYearNotes->setText(QString::number(sYear));
        ui->labelMonthNotes->setText(QString::number(sMonth));
    }

    // mark days with notes
    {
        QStringList *list = db->daysWithNotesInTheMonth(date);
        ui->calendar->setDateTextFormat(QDate(), QTextCharFormat()); // clear

        QTextCharFormat fmt;
        fmt.setFontWeight(QFont::Bold);
        fmt.setFontUnderline(true);

        foreach(const QString & str, *list)
        {
            QDate date = QDate::fromString(str, DATE_FORMAT);
            ui->calendar->setDateTextFormat(date, fmt);
        }

        delete list;
    }

    updateNotesCount();
}

void MainWindow::updateNotesCount()
{
    QAbstractItemModel *model = ui->notesView->model();
    int numNotes = model ? model->rowCount() : 0;
    ui->labelNumNotes->setText(tr("Notes: %1").arg(QString::number(numNotes)));
}

void MainWindow::calendarPageChanged(int year, int month)
{
    updateJournalInfo(QDate(year, month, 1));
}

void MainWindow::setupCategoriesView()
{
    categoriesModel.reset(db->makeCategoriesModel());
    ui->categoriesView->setModel(categoriesModel.data());
    ui->categoriesView->hideColumn(ID_CAT_COL);
}

void MainWindow::setupNotesView(QSqlTableModel *model)
{
    ui->notesView->setModel(model);
    ui->notesView->hideColumn(ID_COL);
    ui->notesView->setSortingEnabled(false); // makes the view to sort again
    ui->notesView->setSortingEnabled(true);
    ui->notesView->resizeColumnsToContents();

    // tweak the 'note name' column width
    int width = ui->notesView->columnWidth(NAME_COL);
    width += COL_WIDTH_GAP;

    if(width > COL_MAX_WIDTH) width = COL_MAX_WIDTH;

    ui->notesView->setColumnWidth(NAME_COL, width);
    ui->notesView->horizontalHeader()->setStretchLastSection(true);

    connect(model, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(updateJournalInfo()));
    updateNotesCount();

}

void MainWindow::setupNotesView(int category_id)
{
    notesModel.reset(db->makeNotesModel(category_id));
    setupNotesView(notesModel.data());
}

void MainWindow::setupNotesView(const QString &date)
{
    notesModel.reset(db->makeNotesModel(date));
    setupNotesView(notesModel.data());
}

void MainWindow::noteAdd()
{
    QDate date;
    int category;

    // if the "Journal" view is selected
    if(ui->toolBox->currentIndex() == TOOLBOX_JOURNAL)
    {
        date = ui->calendar->selectedDate();
        category = DEFAULT_CATEGORY;
    }
    else
    {
        date = QDate::currentDate();
        category = getSelectedCategoryID();
    }

    if(date.isNull())
        date = QDate::currentDate();

    if(category == -1)
        category = DEFAULT_CATEGORY;

    db->noteAdd(category, options->defaultName, date);
    notesModel->select();
    updateJournalInfo();
}

void MainWindow::noteDelete()
{
    QList<int> IDs = getSelectedNotesID();

    if(IDs.empty())
        return;

    QString msg = tr("Permanently delete the selected note(s)?");
    QMessageBox::StandardButtons btns = QMessageBox::Yes | QMessageBox::No;

    if(QMessageBox::question(this, tr("Confirmation"), msg, btns, QMessageBox::No) == QMessageBox::Yes)
    {
        foreach(int note_id, IDs) {
            db->noteDelete(note_id);
        }

        notesModel->select();
        updateJournalInfo();
    }
}

void MainWindow::noteEdit(const QModelIndex &index)
{
    if(index.isValid() && index.column() == CONTENT_COL)
    {
        QSqlTableModel *model;

        if(ui->toolBox->currentIndex() == TOOLBOX_SEARCH)
            model = searchModel.data();
        else
            model = notesModel.data();

        QString curText = model->record(index.row()).value(CONTENT_COL).toString();
        QString newText = editor->exec(curText, options->spellCommand, options->spellDicts);

        if(!newText.isNull())
            model->setData(index, newText);

        model->submitAll();
    }
}

void MainWindow::makeRecurrent()
{
    QModelIndex index = ui->notesView->selectionModel()->currentIndex();
    if(!index.isValid())
        return;

    QString msg = tr("Select the number of (extra) months to recur");
    bool ok;
    int months = QInputDialog::getInt(this, tr("Monthly recurrence"), msg, 12, 1, 60, 1, &ok);

    if(ok)
    {
        QSqlRecord rec  = notesModel->record(index.row());
        int category    = rec.value(CATEGORY_COL).toInt();
        QString name    = rec.value(NAME_COL).toString();
        QDate date      = rec.value(DATE_COL).toDate();
        QString content = rec.value(CONTENT_COL).toString();

        for(int m = 1; m <= months; ++m)
        {
            // always the last valid day
            QDate futureDate = date.addMonths(m);
            db->noteAdd(category, name, futureDate, content);
        }

        notesModel->select();
        updateJournalInfo();
    }
}

int MainWindow::getSelectedCategoryID()
{
    QModelIndex index = ui->categoriesView->selectionModel()->currentIndex();

    if(index.isValid())
        return categoriesModel->record(index.row()).value(ID_CAT_COL).toInt();
    else
        return -1;
}

QList<int> MainWindow::getSelectedNotesID()
{
    QModelIndexList indexes = ui->notesView->selectionModel()->selectedIndexes();
    QList<int> IDs;

    foreach(QModelIndex index, indexes) {
        if(index.isValid())
            IDs.append(notesModel->record(index.row()).value(ID_COL).toInt());
    }

    return IDs;
}

void MainWindow::categoryAdd()
{
    db->categoryAdd(tr("New category"));
    categoriesModel->select();
}

void MainWindow::categoryDelete()
{
    int category_id = getSelectedCategoryID();

    if(category_id < 0)
        return;

    QString msg = tr("Permanently delete the selected category?");
    QMessageBox::StandardButtons btns = QMessageBox::Yes | QMessageBox::No;

    if(QMessageBox::question(this, tr("Confirmation"), msg, btns, QMessageBox::No) ==
            QMessageBox::Yes)
    {
        db->categoryDelete(category_id);
        categoriesModel->select();
        ui->notesView->setModel(nullptr);
        updateJournalInfo();
    }
}

void MainWindow::showOptions()
{
    bool accepted = optionsDialog->execute(options.data());

    if(accepted)
        run(); // use the new options
}

void MainWindow::showAbout()
{
    QString text = QString::fromUtf8(
                       "<html><b>QJournal</b> - Journal and notes manager.<br/> <br/>"
                       "<a href=http://oproj.tuxfamily.org>OProj - Website</a><br/>"
                       "<a href=https://bitbucket.org/lucashnegri/qjournal>Bitbucket - Get the code</a><br/>"
                       "<br/><i> Lucas Hermann Negri </i> - <a "
                       "href=\"mailto:lucashnegri@gmail.com\">lucashnegri@gmail.com</a> <br/>"
                       "<br/><small>Uses the <a "
                       "href=\"http://gnome-look.org/content/show.php/Cheser+Icons?content=113386\">Cheser "
                       "icons</a> as fallback (<a href=\"http://creativecommons.org/licenses/by-sa/3.0/\">CC "
                       "BY-SA "
                       "3.0</a>)</small><br/>"
                       "</html>");

    QMessageBox::about(this, tr("About QJournal"), text);
}

void MainWindow::showAboutQt()
{
    QMessageBox::aboutQt(this, tr("About Qt..."));
}

void MainWindow::searchFromChanged(QDate from)
{
    QDate to = ui->searchTo->date();

    if(to < from)
        ui->searchTo->setDate(from);
}

void MainWindow::doSearch()
{
    QString from = ui->searchFrom->date().toString(DATE_FORMAT);
    QString to = ui->searchTo->date().toString(DATE_FORMAT);
    QString name = ui->searchName->text().trimmed();
    QString category = ui->searchCategory->text().trimmed();
    QString contents = ui->searchContent->text().trimmed();

    searchModel.reset(db->makeSearchModel(from, to, name, category, contents));
    setupNotesView(searchModel.data());
}
