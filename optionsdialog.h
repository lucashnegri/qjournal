#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>

class Options;

namespace Ui
{
class OptionsDialog;
}

/**
 * @brief Exposes the available options to the user.
 */
class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = nullptr);
    ~OptionsDialog();

    /* returns true if the options may have changed */
    bool execute(Options *options);

    private
slots:
    void selectDataBasePath();

private:
    Q_DISABLE_COPY(OptionsDialog)

    bool testPath(const QString &path);
    Ui::OptionsDialog* ui;
};

#endif // OPTIONSDIALOG_H
