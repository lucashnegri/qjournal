#include <QCloseEvent>
#include <QAction>
#include <QPixmap>
#include <QActionGroup>
#include <QInputDialog>
#include <QColor>
#include <QColorDialog>
#include <QFont>
#include <QSpinBox>
#include <QFontComboBox>
#include <QTextList>

#include <QPrintDialog>
#include <QPrinter>

#include "spelldialog.h"
#include "finddialog.h"
#include "noteeditor.h"
#include "ui_noteeditor.h"

NoteEditor::NoteEditor(QWidget *parent) : QMainWindow(parent), ui(new Ui::NoteEditor())
{
    ui->setupUi(this);
    setupToolBar();
    spellDialog = new SpellDialog(this);
    findDialog = new FindDialog(this);
    ui->textEdit->setAcceptRichText(false);
}

NoteEditor::~NoteEditor()
{
    delete ui;
}

void NoteEditor::setupToolBar()
{
    // font selector
    comboFont = new QFontComboBox(this);
    ui->toolBar->addWidget(comboFont);
    connect(comboFont, SIGNAL(activated(QString)), this, SLOT(textFamily(QString)));

    // font size selector
    spinSize = new QSpinBox(this);
    spinSize->setMinimum(1);
    spinSize->setMaximum(64);
    ui->toolBar->addWidget(spinSize);
    connect(spinSize, SIGNAL(valueChanged(int)), this, SLOT(textSize(int)));

    // color selector
    QPixmap pix(ICON_SIZE, ICON_SIZE);
    pix.fill(Qt::black);
    actionTextColor = new QAction(pix, tr("&Color..."), this);
    connect(actionTextColor, SIGNAL(triggered()), this, SLOT(textColor()));
    ui->toolBar->addAction(actionTextColor);

    // group all text-justify actions
    QActionGroup *justifyGroup = new QActionGroup(this);
    justifyGroup->addAction(ui->actionAlignLeft);
    justifyGroup->addAction(ui->actionAlignRight);
    justifyGroup->addAction(ui->actionAlignCenter);
    connect(justifyGroup, SIGNAL(triggered(QAction *)), this, SLOT(textAlign(QAction *)));
}

QDialogButtonBox *NoteEditor::getButtons()
{
    return ui->buttons;
}

QString NoteEditor::getHtml()
{
    return ui->textEdit->toHtml();
}

void NoteEditor::setContentFont(const QFont &font)
{
    ui->textEdit->setFont(font);
}

void NoteEditor::setHtml(const QString &html)
{
    fontChanged(ui->textEdit->font());
    ui->textEdit->setHtml(html);
    ui->textEdit->document()->setModified(false);
    ui->actionUndo->setEnabled(false);
    ui->actionRedo->setEnabled(false);
}

void NoteEditor::setSpellCommands(const QString &command, const QString &dicts)
{
    spellCommand = command;
    spellDicts = dicts;
}

bool NoteEditor::isModified()
{
    return ui->textEdit->document()->isModified();
}

void NoteEditor::textBold()
{
    QTextCharFormat fmt;
    fmt.setFontWeight(ui->actionBold->isChecked() ? QFont::Bold : QFont::Normal);
    mergeFormat(fmt);
}

void NoteEditor::textUnderline()
{
    QTextCharFormat fmt;
    fmt.setFontUnderline(ui->actionUnderline->isChecked());
    mergeFormat(fmt);
}

void NoteEditor::textItalic()
{
    QTextCharFormat fmt;
    fmt.setFontItalic(ui->actionItalic->isChecked());
    mergeFormat(fmt);
}

void NoteEditor::textStrikethrough()
{
    QTextCharFormat fmt;
    fmt.setFontStrikeOut(ui->actionStrikethrough->isChecked());
    mergeFormat(fmt);
}

void NoteEditor::mergeFormat(const QTextCharFormat &format)
{
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.mergeCharFormat(format);
    ui->textEdit->mergeCurrentCharFormat(format);
}

void NoteEditor::fontChanged(const QFont &f)
{
    comboFont->setCurrentIndex(comboFont->findText(QFontInfo(f).family()));
    spinSize->setValue(f.pointSize());
    ui->actionBold->setChecked(f.bold());
    ui->actionItalic->setChecked(f.italic());
    ui->actionUnderline->setChecked(f.underline());
    ui->actionStrikethrough->setChecked(f.strikeOut());
}

void NoteEditor::alignmentChanged(Qt::Alignment a)
{
    if(a & Qt::AlignLeft)
    {
        ui->actionAlignLeft->setChecked(true);
    }
    else if(a & Qt::AlignHCenter)
    {
        ui->actionAlignCenter->setChecked(true);
    }
    else if(a & Qt::AlignRight)
    {
        ui->actionAlignRight->setChecked(true);
    }
}

void NoteEditor::textAlign(QAction *a)
{
    if(a == ui->actionAlignLeft)
        ui->textEdit->setAlignment(Qt::AlignLeft);
    else if(a == ui->actionAlignCenter)
        ui->textEdit->setAlignment(Qt::AlignHCenter);
    else if(a == ui->actionAlignRight)
        ui->textEdit->setAlignment(Qt::AlignRight);

    ui->textEdit->update();
}

void NoteEditor::textFamily(const QString &family)
{
    QTextCharFormat fmt;
    fmt.setFontFamily(family);
    mergeFormat(fmt);
}

void NoteEditor::textSize(int size)
{
    QTextCharFormat fmt;
    fmt.setFontPointSize(size);
    mergeFormat(fmt);
}

void NoteEditor::cursorPositionChanged()
{
    const QTextCursor cursor = ui->textEdit->textCursor();
    const QTextCharFormat fmt = cursor.charFormat();
    fontChanged(fmt.font());
    colorChanged(fmt.foreground().color());
    alignmentChanged(ui->textEdit->alignment());
    listFormatChanged(cursor.currentList());
}

void NoteEditor::textColor()
{
    QColor col = QColorDialog::getColor(ui->textEdit->textColor(), this);

    if(!col.isValid())
        return;

    QTextCharFormat fmt;
    fmt.setForeground(col);
    mergeFormat(fmt);
    colorChanged(col);
}

void NoteEditor::spellCheck()
{
    spellDialog->exec(ui->textEdit, spellCommand, spellDicts);
}

void NoteEditor::findReplace()
{
    findDialog->exec(ui->textEdit);
}

void NoteEditor::printNote()
{
    QPrinter printer;
    QPrintDialog dialog(&printer, this);

    if(dialog.exec() == QDialog::Accepted)
        ui->textEdit->print(&printer);
}

void NoteEditor::orderedList()
{
    if(ui->actionOrdered_List->isChecked())
        setListStyle(ListNumber);
    else
        setListStyle(ListNone);

    ui->actionUnordered_List->setChecked(false);
}

void NoteEditor::unorderedList()
{
    if(ui->actionUnordered_List->isChecked())
        setListStyle(ListBullet);
    else
        setListStyle(ListNone);

    ui->actionOrdered_List->setChecked(false);
}

void NoteEditor::colorChanged(const QColor &c)
{
    QPixmap pix(ICON_SIZE, ICON_SIZE);
    pix.fill(c);
    actionTextColor->setIcon(pix);
}

void NoteEditor::setListStyle(const NoteEditor::ListStyle style)
{
    QTextCursor cursor = ui->textEdit->textCursor();

    if(style != ListNone)
    {
        // merge list style
        cursor.beginEditBlock();
        QTextBlockFormat blockFmt = cursor.blockFormat();
        QTextListFormat listFmt;

        if(cursor.currentList())
        {
            listFmt = cursor.currentList()->format();
        }
        else
        {
            listFmt.setIndent(blockFmt.indent() + 1);
            blockFmt.setIndent(0);
            cursor.setBlockFormat(blockFmt);
        }

        listFmt.setStyle(style == ListBullet ?
                         QTextListFormat::ListDisc :
                         QTextListFormat::ListDecimal);

        cursor.createList(listFmt);
        cursor.endEditBlock();
    }
    else
    {
        // remove format
        QTextBlockFormat bfmt;
        bfmt.setObjectIndex(0);
        cursor.mergeBlockFormat(bfmt);
        ui->textEdit->setTextCursor(cursor);
    }
}

void NoteEditor::listFormatChanged(const QTextList *list)
{
    bool ordered = false, unordered = false;

    if(list)
    {
        if(list->format().style() == QTextListFormat::ListDecimal)
            ordered = true;
        else
            unordered = true;
    }

    ui->actionOrdered_List->setChecked(ordered);
    ui->actionUnordered_List->setChecked(unordered);
}
