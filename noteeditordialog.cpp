#include <QVBoxLayout>
#include <QDialogButtonBox>
#include <QMessageBox>
#include <QPushButton>
#include <QByteArray>
#include <QFont>
#include <QKeyEvent>

#include "noteeditordialog.h"
#include "noteeditor.h"

NoteEditorDialog::NoteEditorDialog(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout *layout = new QVBoxLayout();
    editor = new NoteEditor(parent);
    layout->addWidget(editor);
    layout->setMargin(0);
    setLayout(layout);

    QDialogButtonBox *buttons = editor->getButtons();
    buttons->button(QDialogButtonBox::Save)->setText(tr("&Save"));
    buttons->button(QDialogButtonBox::Save)->setToolTip(tr("Save changes to the database and close the editor."));

    buttons->button(QDialogButtonBox::Cancel)->setText(tr("&Cancel"));
    buttons->button(QDialogButtonBox::Cancel)->setToolTip(tr("Discard changes and close the editor."));

    connect(buttons->button(QDialogButtonBox::Save), SIGNAL(clicked()), this, SLOT(accept()));
    connect(buttons->button(QDialogButtonBox::Cancel), SIGNAL(clicked()), this, SLOT(reject()));

    setWindowTitle(tr("Note editor"));
}

QByteArray NoteEditorDialog::saveState()
{
    return editor->saveState();
}

bool NoteEditorDialog::restoreState(const QByteArray &array)
{
    return editor->restoreState(array);
}

void NoteEditorDialog::setContentFont(const QFont &font)
{
    editor->setContentFont(font);
}

QString NoteEditorDialog::exec(const QString &content, const QString &spellCommand,
                               const QString &spellDicts)
{
    editor->setHtml(content);
    editor->setSpellCommands(spellCommand, spellDicts);
    return QDialog::exec() == QDialog::Accepted ? editor->getHtml() : QString();
}

void NoteEditorDialog::reject()
{
    bool canClose;

    if(editor->isModified())
    {
        int resp = QMessageBox::question(this, tr("Confirmation"),
                                         tr("Close editor and discard changes?"),
                                         QMessageBox::Yes | QMessageBox::No);
        canClose = resp == QMessageBox::Yes;
    }
    else
    {
        canClose = true;
    }

    if(canClose)
        QDialog::reject();
}

void NoteEditorDialog::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
        return;

    QDialog::keyPressEvent(event);
}
