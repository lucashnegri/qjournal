#ifndef OPTIONS_H
#define OPTIONS_H

class QSettings;

#include <QFont>

/* holds all the program options (configurable by the user) in one place */
struct Options
{
    Options();

    void loadFromSettings(QSettings &settings);
    void saveToSettings(QSettings &settings) const;

    QString dbPath;
    QString defaultName;
    QString spellCommand;
    QString spellDicts;
    QFont contentFont;

private:
    QString getStorageLocation();
};

#endif // OPTIONS_H
