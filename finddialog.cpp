#include <QTextEdit>

#include "finddialog.h"
#include "ui_finddialog.h"

FindDialog::FindDialog(QWidget *parent) : QDialog(parent), ui(new Ui::FindDialog())
{
    ui->setupUi(this);
}

FindDialog::~FindDialog()
{
    delete ui;
}

void FindDialog::exec(QTextEdit *edit)
{
    this->edit = edit;
    show();
}

void FindDialog::findPrevious()
{
    find(true);
}

void FindDialog::findNext()
{
    find(false);
}

QTextDocument::FindFlags FindDialog::getFlags(bool previous)
{
    bool whole = ui->checkWhole->isChecked();
    bool sensitive = ui->checkSensitive->isChecked();

    QTextDocument::FindFlags options = nullptr;

    if(previous)
        options |= QTextDocument::FindBackward;

    if(whole)
        options |= QTextDocument::FindWholeWords;

    if(sensitive)
        options |= QTextDocument::FindCaseSensitively;

    return options;
}

void FindDialog::find(bool previous)
{
    QString needle = ui->lineFind->text();
    edit->find(needle, getFlags(previous));
}

void FindDialog::replace()
{
    QString newWord = ui->lineReplace->text();

    // if there is a selection, then replace it with newWord
    QTextCursor cursor = edit->textCursor();

    if(cursor.hasSelection())
        cursor.insertText(newWord);

    // find and select next occurence
    findNext();
}
void FindDialog::replaceAll()
{
    QString oldWord = ui->lineFind->text();
    QString newWord = ui->lineReplace->text();

    QTextDocument::FindFlags options = getFlags(false);

    QTextCursor cursor = edit->textCursor();
    cursor.beginEditBlock();

    while(edit->find(oldWord, options))
    {
        QTextCursor qc = edit->textCursor(); // must be a new cursor

        if(qc.hasSelection())
            qc.insertText(newWord);
    }

    cursor.endEditBlock();
}
