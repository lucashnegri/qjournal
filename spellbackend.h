#ifndef SPELLBACKEND_H
#define SPELLBACKEND_H

class QStringList;
#include <QString>

/* backend for ispell compatible spell checkers, such as aspell */
class SpellBackend
{
public:
    SpellBackend();

    void setCommands(const QString &command = QString(), const QString &spellDicts = QString());
    void setLanguage(const QString &language = QString());

    /**
     * @brief Calls the spell checker and returns if the word is correct. If the word is not
     * correct,
     * then the list will be populted with the suggestions (the list is always cleared).
     * If an error occured (as no spell checker available, ok will be set to false (being set to
     * true otherwise).
     */
    bool correct(const QString &word, QStringList &list, bool &ok);

    /**
     * @brief Returns a list with the raw name of the available languages to the spell checker.
     * If an error occured (as no spell checker available, ok will be set to false (being set to
     * true otherwise).
     */
    QStringList getAvailableLanguages(bool &ok);

private:
    QString command, spellDicts, language;
};

#endif // SPELLBACKEND_H
