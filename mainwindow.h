#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScopedPointer>
#include <QDate>
#include <QList>

class OptionsDialog;
class NoteEditorDialog;
class Database;
class QSqlTableModel;
class QModelIndex;
struct Options;

namespace Ui
{
class MainWindow;
}

/**
 * @brief Main application window.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void run();

protected:
    void closeEvent(QCloseEvent *event);

    private
slots:
    void categoryActivated(const QModelIndex &index);
    void calendarPageChanged(int year, int month);
    void dateSelected();
    void updateJournalInfo(QDate date = QDate());
    void showOptions();

    void categoryAdd();
    void categoryDelete();

    void noteAdd();
    void noteDelete();
    void noteEdit(const QModelIndex &index);
    void makeRecurrent();

    void showAbout();
    void showAboutQt();
    void searchFromChanged(QDate);
    void doSearch();

private:
    Q_DISABLE_COPY(MainWindow)

    void setupCategoriesView();
    void setupNotesView(int category_id);
    void setupNotesView(const QString &date);
    void setupNotesView(QSqlTableModel *model);
    void updateNotesCount();

    void save();
    void restore();

    int getSelectedCategoryID();
    QList<int> getSelectedNotesID();

    QString getStorageLocation();

    QScopedPointer<Options> options;
    QScopedPointer<QSqlTableModel> categoriesModel;
    QScopedPointer<QSqlTableModel> notesModel;
    QScopedPointer<QSqlTableModel> searchModel;

    // only one instance, managed by QObject hierarchy
    NoteEditorDialog *editor;
    OptionsDialog *optionsDialog;
    Database *db;

    // managed by the destructor
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
