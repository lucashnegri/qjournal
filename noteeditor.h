#ifndef NOTEEDITOR_H
#define NOTEEDITOR_H

#include <QMainWindow>

class QDialogButtonBox;
class QTextCharFormat;
class QFont;
class QSpinBox;
class QFontComboBox;
class SpellDialog;
class FindDialog;
class QTextList;

const int ICON_SIZE = 16;

namespace Ui
{
class NoteEditor;
}

/**
 * @brief Note content editor window. Handles the rich text editing of the note contents.
 */
class NoteEditor : public QMainWindow
{
    Q_OBJECT

    enum ListStyle
    {
        ListNone   = 0,
        ListBullet,
        ListNumber
    };

public:
    explicit NoteEditor(QWidget *parent = 0);
    ~NoteEditor();

    void editText(QWidget *caller, const QString &content);
    QDialogButtonBox *getButtons();

    QString getHtml();
    void setHtml(const QString &html);

    void setSpellCommands(const QString &command, const QString &dicts);

    void setContentFont(const QFont &font);
    bool isModified();

    private
slots:
    void textItalic();
    void textUnderline();
    void textBold();
    void textStrikethrough();
    void textAlign(QAction *a);
    void textFamily(const QString &family);
    void textSize(int size);
    void textColor();
    void cursorPositionChanged();
    void spellCheck();
    void findReplace();
    void printNote();
    void orderedList();
    void unorderedList();

private:
    Q_DISABLE_COPY(NoteEditor)

    void setupToolBar();
    void fontChanged(const QFont &f);
    void alignmentChanged(Qt::Alignment a);
    void mergeFormat(const QTextCharFormat &format);
    void colorChanged(const QColor &c);
    void setListStyle(const ListStyle style);
    void listFormatChanged(const QTextList* list);

    QString spellCommand, spellDicts;

    SpellDialog *spellDialog;
    FindDialog *findDialog;
    QAction *actionTextColor;
    QSpinBox *spinSize;
    QFontComboBox *comboFont;
    Ui::NoteEditor *ui;
};

#endif // NOTEEDITOR_H
