#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QFont>

#include "options.h"
#include "config.h"
#include "optionsdialog.h"
#include "ui_optionsdialog.h"

OptionsDialog::OptionsDialog(QWidget *parent) : QDialog(parent), ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

bool OptionsDialog::execute(Options *options)
{
    // set values from the options
    ui->lineDatabaseFile->setText(options->dbPath);
    ui->fontCombo->setCurrentFont(options->contentFont);
    ui->spinSize->setValue(options->contentFont.pointSize());
    ui->lineName->setText(options->defaultName);
    ui->lineSpell->setText(options->spellCommand);
    ui->lineDicts->setText(options->spellDicts);

    // let the user change the values
    int response = QDialog::exec();

    if(response == QDialog::Accepted)
    {
        // user has accepted the changes, save it to options
        options->dbPath = ui->lineDatabaseFile->text();
        options->contentFont = ui->fontCombo->currentFont();

        options->contentFont.setPointSize(ui->spinSize->value());
        options->defaultName = ui->lineName->text();
        options->spellCommand = ui->lineSpell->text();
        options->spellDicts = ui->lineDicts->text();
        return true;
    }
    else
    {
        return false;
    }
}

bool OptionsDialog::testPath(const QString &path)
{

    QFile f(path);

    if(f.exists())
    {
        return true;
    }
    else   // file does not exists, ensure that you we can write to it
    {
        if(f.open(QFile::WriteOnly))
        {
            f.remove();
            return true;
        }
    }

    return false;
}

void OptionsDialog::selectDataBasePath()
{
    QString path =
        QFileDialog::getSaveFileName(this, tr("Database file..."), ui->lineDatabaseFile->text(),
                                     QString(), nullptr, QFileDialog::DontConfirmOverwrite);

    if(!path.isNull() && !path.isEmpty())
    {
        if(testPath(path))
            ui->lineDatabaseFile->setText(path);
        else
            QMessageBox::warning(this, tr("Choose another file"),
                                 tr("Could not read/write to the selected file"));
    }
}
