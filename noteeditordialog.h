#ifndef NOTEEDITORDIALOG_H
#define NOTEEDITORDIALOG_H

#include <QDialog>

class QByteArray;
class QFont;
class QKeyEvent;
class NoteEditor;

/**
 * @brief Interfaces the content editing between the caller and note editor.
 */
class NoteEditorDialog : public QDialog
{
    Q_OBJECT
public:
    explicit NoteEditorDialog(QWidget *parent = nullptr);

    QByteArray saveState();
    bool restoreState(const QByteArray &array);

    void setContentFont(const QFont &font);
    QString exec(const QString &content, const QString &spellCommand = QString(),
                 const QString &spellDicts = QString());

    public
slots:
    void reject();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Q_DISABLE_COPY(NoteEditorDialog)
    NoteEditor *editor;
};

#endif // NOTEEDITORDIALOG_H
