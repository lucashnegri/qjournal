#include <QProcess>
#include <QIODevice>
#include <QLocale>
#include <QStringList>

// std for the rescue
#include <sstream>
#include <string>

#include "config.h"
#include "spellbackend.h"

SpellBackend::SpellBackend()
{
    setCommands();
    setLanguage();
}

bool SpellBackend::correct(const QString &word, QStringList &list, bool &ok)
{
    QProcess spell;
    spell.start(command.arg(language), QProcess::ReadWrite);
    ok = true;
    list.clear();

    if(!spell.waitForStarted(SPELL_TIMEOUT))
    {
        ok = false;
        return false;
    }

    spell.write(word.toLocal8Bit());
    spell.closeWriteChannel();

    if(!spell.waitForFinished(SPELL_TIMEOUT))
    {
        ok = false;
        return false;
    }

    spell.readLine(); /* ignore the header line */
    QByteArray out = spell.readAll();
    std::stringstream stream;
    stream << out.data();

    // parse the output

    // 1: spelling status: * = ok, & = suggestions, # = none
    std::string status;
    stream >> status;

    if(status == "*")
        return true; // word is correct

    if(status == "+")
        return true; // word is correct (and have a 'root' word)

    if(status == "#")
        return false;  // word is wrong

    if(status != "&")  // invalid response
    {
        ok = false;
        return false;
    }

    // ok, the word was marked as "wrong", and we have some suggestions
    std::string garbage;
    stream >> garbage;

    int count, offset;
    stream >> count >> offset;
    stream >> garbage;

    for(int i = 0; i < count; ++i)
    {
        std::string word;
        bool last = i == (count - 1);
        getline(stream, word, last ? '\n' : ',');
        list.append(QString::fromUtf8(word.c_str()).trimmed());
    }

    return false; // the word is "wrong"
}

QStringList SpellBackend::getAvailableLanguages(bool &ok)
{
    // launch the read only process
    QProcess spell;
    spell.start(spellDicts, QProcess::ReadOnly);
    ok = true;

    QStringList list;

    if(!spell.waitForStarted(SPELL_TIMEOUT) || !spell.waitForFinished(SPELL_TIMEOUT))
    {
        // not ok!
        ok = false;
    }
    else
    {
        // ok, populate the list
        while(true)
        {
            QByteArray line = spell.readLine().trimmed();

            if(line.isNull() || line.isEmpty())
                break;

            list.push_back(QString(line));
        }
    }

    return list;
}

void SpellBackend::setCommands(const QString &command, const QString &spellDicts)
{
    this->command = (command.isNull() || command.isEmpty()) ? SPELL_DEFAULT : command;
    this->spellDicts = (spellDicts.isNull() || spellDicts.isEmpty()) ? DICTS_DEFAULT : spellDicts;
}

void SpellBackend::setLanguage(const QString &language)
{
    if(language.isNull() || language.isEmpty())
        this->language = QLocale::system().name();
    else
        this->language = language;
}
