#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QDialog>
#include <QTextDocument>

class QTextEdit;

namespace Ui
{
class FindDialog;
}

class FindDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FindDialog(QWidget *parent = nullptr);
    ~FindDialog();
    void exec(QTextEdit *edit);

    public
slots:
    void findPrevious();
    void findNext();
    void replace();
    void replaceAll();

private:
    Q_DISABLE_COPY(FindDialog)

    void find(bool previous);
    QTextDocument::FindFlags getFlags(bool previous);

    Ui::FindDialog *ui;
    QTextEdit *edit;
};

#endif // FINDDIALOG_H
