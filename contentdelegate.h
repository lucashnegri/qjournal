#ifndef CONTENTDELEGATE_H
#define CONTENTDELEGATE_H

#include <QStyledItemDelegate>

/**
 * @brief Delegate for the "content" column. Shows a placeholder (...) and does not allow inline
 * editing.
 */
class ContentDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    explicit ContentDelegate(QObject *parent = nullptr);
    QString displayText(const QVariant &value, const QLocale &locale) const;
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                          const QModelIndex &index) const;
private:
    Q_DISABLE_COPY(ContentDelegate)
};

#endif // CONTENTDELEGATE_H
