About QJournal
==============

Description
-----------
    
[QJourna][1] is a personal journal and notes manager. The main objectives are to be be simple to use, while
providing some interesting features (rich text, fast and intuitive interface).

Performance
-----------

QJournal should work fine even if you write 10 notes per day, every day, for an entire lifetime.

Shortcuts
---------

Note: the shortcuts are translatable and may change according to the language.

    Bold                Ctrl+B
    Italic              Ctrl+I
    Underline           Ctrl+U
    Striketrough        Ctrl+T
    Undo                Ctrl+Z
    Redo                Ctrl+Shift+Z
    Spell checking      Ctrl+S
    Find or replace     Ctrl+F
    Print note          Ctrl+P
    Ordered list        Ctrl+O
    Unordered list      Ctrl+L

License
-----------

GPLv3+.

Contact
-----------

Lucas Hermann Negri - lucashnegri@gmail.com

[1]: https://bitbucket.org/lucashnegri/qjournal
