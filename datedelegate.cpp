#include <QDateEdit>

#include "datedelegate.h"
#include "config.h"

DateDelegate::DateDelegate(QObject *parent) : QStyledItemDelegate(parent)
{
}

QWidget *DateDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                    const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);
    return new QDateEdit(parent);
}

void DateDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QString raw = index.model()->data(index, Qt::EditRole).toString();
    QDateEdit *dateEdit = static_cast<QDateEdit *>(editor);
    dateEdit->setDate(QDate::fromString(raw, DATE_FORMAT));
}

void DateDelegate::setModelData(QWidget *editor, QAbstractItemModel *model,
                                const QModelIndex &index) const
{
    QDateEdit *dateEdit = static_cast<QDateEdit *>(editor);
    dateEdit->interpretText();
    model->setData(index, dateEdit->date(), Qt::EditRole);
}

void DateDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option,
                                        const QModelIndex &index) const
{
    Q_UNUSED(index);
    editor->setGeometry(option.rect);
}
