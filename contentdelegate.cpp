#include "contentdelegate.h"

ContentDelegate::ContentDelegate(QObject *parent) : QStyledItemDelegate(parent)
{
}

QString ContentDelegate::displayText(const QVariant &value, const QLocale &locale) const
{
    Q_UNUSED(value)
    Q_UNUSED(locale)
    return tr("...");
}

QWidget *ContentDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option,
                                       const QModelIndex &index) const
{
    Q_UNUSED(parent)
    Q_UNUSED(option)
    Q_UNUSED(index)
    return nullptr;
}
