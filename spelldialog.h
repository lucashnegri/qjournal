#ifndef SPELLDIALOG_H
#define SPELLDIALOG_H

#include <QDialog>
#include <QHash>
#include <QScopedPointer>
#include "spellbackend.h"

class QTextEdit;
class QModelIndex;
class QListWidgetItem;

namespace Ui
{
class SpellDialog;
}

class SpellDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SpellDialog(QWidget *parent = nullptr);
    ~SpellDialog();
    void exec(QTextEdit *edit, const QString &spellCommand, const QString &spellDicts);

    private
slots:
    void replace();
    void replaceAll();
    void ignore();
    void ignoreAll();
    void suggestionClicked(QListWidgetItem *item);
    void languageChanged(QString language);

private:
    Q_DISABLE_COPY(SpellDialog)

    void correctLoop();
    bool correctCurrent();
    void beginCheck();
    void checkDone();
    bool moveNext();

    bool started;

    QHash<QString, bool> ignoredWords;
    QHash<QString, QString> replaceAllWords;

    QScopedPointer<SpellBackend> spell;
    Ui::SpellDialog *ui;
    QTextEdit *edit; // not managed here, must last until the dialog returns
};

#endif // SPELLDIALOG_H
