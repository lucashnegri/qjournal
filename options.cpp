#include <QtGlobal>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QDesktopServices>
#else
#include <QStandardPaths>
#endif

#include <QSettings>
#include <QDir>

#include "options.h"
#include "config.h"

Options::Options()
{
}

void Options::loadFromSettings(QSettings &settings)
{
    // dbPath
    dbPath = settings.value("dbPath").toString();

    if(dbPath.isNull())  // initialize to a default value if needed
    {
        // first application run, get the default database path
        QString path = getStorageLocation();

        QDir().mkpath(path);
        dbPath = path + "/default.db3";
        settings.setValue("dbPath", dbPath);
    }

    // defaultName
    defaultName = settings.value("defaultName").toString();

    if(defaultName.isNull())
        defaultName = QObject::tr("Untitled");

    // font
    QVariant editorFontFamily = settings.value("editorFontFamily");
    QVariant editorFontSize = settings.value("editorFontSize");

    if(!editorFontFamily.isNull() || !editorFontSize.isNull())
    {
        // load
        contentFont.setFamily(editorFontFamily.toString());
        contentFont.setPointSize(editorFontSize.toInt());
    }
    else
    {
        // get default
        contentFont = QFont();
    }

    // spellCommand
    spellCommand = settings.value("spellCommand").toString();

    if(spellCommand.isNull())
        spellCommand = SPELL_DEFAULT;

    // spellDicts
    spellDicts = settings.value("spellDicts").toString();

    if(spellDicts.isNull())
        spellDicts = DICTS_DEFAULT;
}

void Options::saveToSettings(QSettings &settings) const
{
    settings.setValue("dbPath", dbPath);
    settings.setValue("defaultName", defaultName);
    settings.setValue("editorFontFamily", contentFont.family());
    settings.setValue("editorFontSize", contentFont.pointSize());
    settings.setValue("spellCommand", spellCommand);
    settings.setValue("spellDicts", spellDicts);
}

QString Options::getStorageLocation()
{
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    return QDesktopServices::storageLocation(QDesktopServices::DataLocation);
#else
    return QStandardPaths::standardLocations(QStandardPaths::DataLocation).first();
#endif
}
