#include <QStringList>
#include <QSqlTableModel>
#include "database.h"
#include "config.h"

Database::Database(QObject *parent) : QObject(parent)
{
}

bool Database::loadOrCreate(const QString &name)
{
    if(db.isOpen())
    {
        // database is already open. Release references and unregister the old one
        QString defaultName = db.database().connectionName();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(defaultName);
    }

    // create a new database connection
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(name);

    if(!db.open())
        return false;

    // initialize the table structure if needed (should be only on the first run)
    if(!db.tables().contains("categories"))
    {
        db.exec(CREATE_CATEGORIES);
        db.exec(CREATE_NOTES);
        db.exec("INSERT INTO categories(category_id, name) VALUES ('0', '" + tr("Journal") +
                "')");
        // SQL injection by a translated string would be cool
    }

    return true;
}

QSqlTableModel *Database::makeCategoriesModel()
{
    QSqlTableModel *model = new QSqlTableModel(nullptr, db);
    model->setTable("categories");
    model->setHeaderData(ID_CAT_COL, Qt::Horizontal, tr("ID"));
    model->setHeaderData(NAME_CAT_COL, Qt::Horizontal, tr("Category"));
    model->setSort(1, Qt::AscendingOrder);
    model->select();

    return model;
}

QSqlTableModel *Database::makeNotesModel()
{
    QSqlRelationalTableModel *model = new QSqlRelationalTableModel(nullptr, db);
    model->setTable("notes");
    model->setHeaderData(ID_COL, Qt::Horizontal, tr("ID"));
    model->setHeaderData(CATEGORY_COL, Qt::Horizontal, tr("Category"));
    model->setHeaderData(NAME_COL, Qt::Horizontal, tr("Note"));
    model->setHeaderData(DATE_COL, Qt::Horizontal, tr("Date"));
    model->setHeaderData(CONTENT_COL, Qt::Horizontal, tr("Contents"), Qt::EditRole);
    model->setRelation(CATEGORY_COL, QSqlRelation("categories", "category_id", "name"));

    return model;
}

QSqlTableModel *Database::makeNotesModel(int category_id)
{
    QSqlTableModel *model = makeNotesModel();
    model->setFilter("notes.category_id=" + QString::number(category_id));
    model->select();

    return model;
}

QSqlTableModel *Database::makeNotesModel(const QString &date)
{
    QSqlTableModel *model = makeNotesModel();
    model->setFilter(QString("date='%1'").arg(date));
    model->select();

    return model;
}

QSqlTableModel *Database::makeSearchModel(const QString &from, const QString &to,
        const QString &name, const QString &category,
        const QString &contents)
{
    // prone to injection, but, why would the user do this?
    QStringList where;

    if(!from.isEmpty() && !to.isEmpty())
        where.append(QString("date BETWEEN '%1' AND '%2'").arg(from, to));

    if(!name.isEmpty())
        where.append(QString("notes.name LIKE '%%1%'").arg(name));

    if(!category.isEmpty())  // hack
        where.append(QString("relTblAl_1.name LIKE '%%1%'").arg(category));

    if(!contents.isEmpty())
        where.append(QString("content LIKE '%%1%'").arg(contents));

    QSqlTableModel *model = makeNotesModel();
    model->setFilter(where.join(" AND "));
    model->select();

    return model;
}

QStringList *Database::daysWithNotesInTheMonth(const QDate &date)
{
    QString nowStr = date.toString(DATE_FORMAT);
    QString sql = QString("SELECT distinct(date) FROM notes WHERE date BETWEEN "
                          "date('%1', 'start of month') AND "
                          "date('%1', 'start of month', '+1 month')").arg(nowStr);
    QSqlQuery query(sql, db);

    QStringList *list = new QStringList();

    while(query.next())
        list->append(query.value(0).toString());

    return list;
}

void Database::noteAdd(int category_id, const QString &name, const QDate &date, const QString &content)
{
    if(category_id < DEFAULT_CATEGORY)
        category_id = DEFAULT_CATEGORY;

    QSqlQuery query;
    query.prepare("INSERT INTO notes(category_id,name,date,content) VALUES(?,?,?,?)");
    query.addBindValue(category_id);
    query.addBindValue(name);
    query.addBindValue(date.toString(DATE_FORMAT));
    query.addBindValue(content);
    query.exec();
}

void Database::noteDelete(int note_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM notes WHERE note_id=?");
    query.addBindValue(note_id);
    query.exec();
}

void Database::categoryAdd(const QString &name)
{
    QSqlQuery query;
    query.prepare("INSERT INTO categories(name) VALUES(?)");
    query.addBindValue(name);
    query.exec();
}

void Database::categoryDelete(int category_id)
{
    // can't delete the default category
    if(category_id == DEFAULT_CATEGORY)
        return;

    // ok, delete associated notes then the category
    {
        QSqlQuery query;
        query.prepare("DELETE FROM notes WHERE category_id=?");
        query.addBindValue(category_id);
        query.exec();
    }
    {
        QSqlQuery query;
        query.prepare("DELETE FROM categories WHERE category_id=?");
        query.addBindValue(category_id);
        query.exec();
    }
}

int Database::notesHelper(const QString &sql)
{
    QSqlQuery query(sql);
    query.next();
    return query.value(0).toInt();
}

void Database::notesStatistics(const QDate &date, int &total, int &year, int &month)
{
    QString nowStr = date.toString(DATE_FORMAT);
    total = notesHelper("SELECT count(*) FROM notes");
    year = notesHelper(QString("SELECT count(*) FROM notes "
                               "WHERE date BETWEEN date('%1', 'start of year') "
                               "AND date('%1', 'start of year', '+1 year')").arg(nowStr));

    month = notesHelper(QString("SELECT count(*) FROM notes "
                                "WHERE date BETWEEN date('%1', 'start of month') "
                                "AND date('%1', 'start of month', '+1 month')").arg(nowStr));
}
