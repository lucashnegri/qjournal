<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR">
<context>
    <name>ContentDelegate</name>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>Database</name>
    <message>
        <source>ID</source>
        <translation>Nº</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>Nota</translation>
    </message>
    <message>
        <source>Journal</source>
        <translation>Diário</translation>
    </message>
    <message>
        <source>Contents</source>
        <translation>Conteúdo</translation>
    </message>
    <message>
        <source>Category</source>
        <translation>Categoria</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <source>Find and replace</source>
        <translation>Localizar e substituir</translation>
    </message>
    <message>
        <source>&amp;Next</source>
        <translation>&amp;Próximo</translation>
    </message>
    <message>
        <source>&amp;Replace</source>
        <translation>&amp;Substituir</translation>
    </message>
    <message>
        <source>&amp;Previous</source>
        <translation>&amp;Anterior</translation>
    </message>
    <message>
        <source>R&amp;eplace:</source>
        <translation>&amp;Substituir:</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <source>Match &amp;whole words only</source>
        <translation>Casar palavras &amp;inteiras somente</translation>
    </message>
    <message>
        <source>Case &amp;sensitive</source>
        <translation>&amp;Diferenciar maiúsculas e minúsculas</translation>
    </message>
    <message>
        <source>F&amp;ind:</source>
        <translation>&amp;Buscar:</translation>
    </message>
    <message>
        <source>Replace &amp;all</source>
        <translation>Substituir &amp;todas</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <source>to</source>
        <translation>até</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>A&amp;juda</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>&amp;Sair</translation>
    </message>
    <message>
        <source>Edi&amp;t</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <source>The database could not be opened.
Configure the database path in the options dialog.</source>
        <translation>O banco de dados não pôde ser aberto.
Configure o caminho do banco de dados no diálogo de opções.</translation>
    </message>
    <message>
        <source>&amp;Journal</source>
        <translation>&amp;Diário</translation>
    </message>
    <message>
        <source>QJournal - %1</source>
        <translation>QJournal - %1</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation>&amp;Sobre</translation>
    </message>
    <message>
        <source>&amp;Usage</source>
        <translation>&amp;Utilização</translation>
    </message>
    <message>
        <source>&amp;Name contains</source>
        <translation>&amp;Nome contém</translation>
    </message>
    <message>
        <source>Untitled</source>
        <translation type="vanished">Sem nome</translation>
    </message>
    <message>
        <source>About Qt...</source>
        <translation>Sobre o Qt...</translation>
    </message>
    <message>
        <source>&amp;Options</source>
        <translation>&amp;Opções</translation>
    </message>
    <message>
        <source>&amp;Category contains</source>
        <translation>&amp;Categoria contém</translation>
    </message>
    <message>
        <source>About &amp;Qt</source>
        <translation>Sobre o &amp;Qt</translation>
    </message>
    <message>
        <source>Notes (year)</source>
        <translation>Notas (ano)</translation>
    </message>
    <message>
        <source>&amp;Delete note</source>
        <translation>&amp;Remover nota</translation>
    </message>
    <message>
        <source>Searc&amp;h</source>
        <translation type="vanished">Busca&amp;r</translation>
    </message>
    <message>
        <source>About QJournal</source>
        <translation>Sobre o QJournal</translation>
    </message>
    <message>
        <source>D&amp;elete Category</source>
        <translation>Re&amp;mover categoria</translation>
    </message>
    <message>
        <source>&amp;Add note</source>
        <translation>&amp;Adicionar nota</translation>
    </message>
    <message>
        <source>Notes: %1</source>
        <translation>Notas: %1</translation>
    </message>
    <message>
        <source>&amp;Export</source>
        <translation>&amp;Exportar</translation>
    </message>
    <message>
        <source>&amp;Period</source>
        <translation>&amp;Período</translation>
    </message>
    <message>
        <source>&amp;Search</source>
        <translation>&amp;Busca</translation>
    </message>
    <message>
        <source>Notes (month)</source>
        <translation>Notas (mês)</translation>
    </message>
    <message>
        <source>&amp;Notes view</source>
        <translation>Visualização de &amp;notas</translation>
    </message>
    <message>
        <source>Cate&amp;gories</source>
        <translation>&amp;Categorias</translation>
    </message>
    <message>
        <source>&amp;Journal view</source>
        <translation>Visualização de &amp;diário</translation>
    </message>
    <message>
        <source>New category</source>
        <translation>Nova categoria</translation>
    </message>
    <message>
        <source>Total notes</source>
        <translation>Total de notas</translation>
    </message>
    <message>
        <source>Add &amp;Category</source>
        <translation>Adicionar ca&amp;tegoria</translation>
    </message>
    <message>
        <source>C&amp;ontent contains</source>
        <translation>C&amp;onteúdo contém</translation>
    </message>
    <message>
        <source>Database error</source>
        <translation>Erro no banco de dados</translation>
    </message>
    <message>
        <source>Permanently delete the selected category?</source>
        <translation>Remover permanentemente a categoria selecionada?</translation>
    </message>
    <message>
        <source>Confirmation</source>
        <translation>Confirmação</translation>
    </message>
    <message>
        <source>Permanently delete the selected note?</source>
        <translation>Remover permanentemente a nota selecionada?</translation>
    </message>
    <message>
        <source>&amp;Make recurrent</source>
        <translation>&amp;Tornar recorrente</translation>
    </message>
    <message>
        <source>Select the number of (extra) months to recur</source>
        <translation>Selecionar o número de meses (extras) para repetir</translation>
    </message>
    <message>
        <source>Monthly recurrence</source>
        <translation>Recorrência mensal</translation>
    </message>
</context>
<context>
    <name>NoteEditor</name>
    <message>
        <source>Bold</source>
        <translation>Negrito</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Refazer</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>Desfazer</translation>
    </message>
    <message>
        <source>Strikethrough</source>
        <translation>Tachado</translation>
    </message>
    <message>
        <source>Underline text</source>
        <translation>Texto sublinhado</translation>
    </message>
    <message>
        <source>Left align text</source>
        <translation>Alinhar texto à esquerda</translation>
    </message>
    <message>
        <source>Center align text</source>
        <translation>Centralizar o texto</translation>
    </message>
    <message>
        <source>Bold text</source>
        <translation>Texto em negrito</translation>
    </message>
    <message>
        <source>Increase text size</source>
        <translation type="vanished">Aumentar o tamanho da fonte</translation>
    </message>
    <message>
        <source>alignCenter</source>
        <translation>Centralizar</translation>
    </message>
    <message>
        <source>Decrease text size</source>
        <translation type="vanished">Diminuir o tamanho da fonte</translation>
    </message>
    <message>
        <source>Right align text</source>
        <translation>Alinhar texto à direita</translation>
    </message>
    <message>
        <source>Strikethrough text</source>
        <translation>Texto tachado</translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation type="vanished">Ctrl++</translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation type="vanished">Ctrl+-</translation>
    </message>
    <message>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <source>Ctrl+D</source>
        <translation type="vanished">Ctrl+D</translation>
    </message>
    <message>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <source>Ctrl+U</source>
        <translation>Ctrl+U</translation>
    </message>
    <message>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <source>Repeat last action</source>
        <translation>Refazer última ação</translation>
    </message>
    <message>
        <source>&amp;Color...</source>
        <translation>&amp;Cor...</translation>
    </message>
    <message>
        <source>Note editor</source>
        <translation>Editor de notas</translation>
    </message>
    <message>
        <source>Undo last action</source>
        <translation>Dezfazer última ação</translation>
    </message>
    <message>
        <source>List item</source>
        <translation type="vanished">Item de lista</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation>Sublinhado</translation>
    </message>
    <message>
        <source>toolBar</source>
        <translation type="vanished">Barra de ferramentas</translation>
    </message>
    <message>
        <source>Ctrl+Shift+Z</source>
        <translation>Ctrl+Shift+Z</translation>
    </message>
    <message>
        <source>alignRight</source>
        <translation>Alinhar à direita</translation>
    </message>
    <message>
        <source>Italic text</source>
        <translation>Texto em itálico</translation>
    </message>
    <message>
        <source>alignLeft</source>
        <translation>Alinhar à esquerda</translation>
    </message>
    <message>
        <source>Spell</source>
        <translation>Grafia</translation>
    </message>
    <message>
        <source>Check the spelling of a word</source>
        <translation>Verificar a grafia de uma palavra</translation>
    </message>
    <message>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <source>Find or replace</source>
        <translation>Localizar e substituir</translation>
    </message>
    <message>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <source>Print the note</source>
        <translation>Imprime a nota</translation>
    </message>
    <message>
        <source>Ctrl+P</source>
        <translation>Ctrl + P</translation>
    </message>
    <message>
        <source>Ordered list</source>
        <translation>Lista ordenada</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <source>Unordered list</source>
        <translation>Lista sem ordem</translation>
    </message>
    <message>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
</context>
<context>
    <name>NoteEditorDialog</name>
    <message>
        <source>Close editor and discard changes?</source>
        <translation>Fechar o editor e descartar alterações?</translation>
    </message>
    <message>
        <source>Note editor</source>
        <translation>Editor de notas</translation>
    </message>
    <message>
        <source>Confirmation</source>
        <translation>Confirmação</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
    <message>
        <source>Save changes to the database and close the editor.</source>
        <translation>Salvar modificações no banco de dados e fechar o editor.</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <source>Discard changes and close the editor.</source>
        <translation>Descartar modificações e fechar o editor.</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <source>Choose another file</source>
        <translation>Escolher outro arquivo</translation>
    </message>
    <message>
        <source>&amp;Database file</source>
        <translation type="vanished">Arquivo do &amp;banco de dados</translation>
    </message>
    <message>
        <source>Could not read/write to the selected file</source>
        <translation>Não foi possível ler/escrever no arquivo selecionado</translation>
    </message>
    <message>
        <source>Default note name</source>
        <translation>Nome padrão da nota</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opções</translation>
    </message>
    <message>
        <source>&amp;Browse...</source>
        <translation>&amp;Escolher...</translation>
    </message>
    <message>
        <source>Path to load/save the database</source>
        <translation>Caminho para salvar / carregar o banco de dados</translation>
    </message>
    <message>
        <source>Database file...</source>
        <translation>Arquivo do banco de dados...</translation>
    </message>
    <message>
        <source>Default font of the content editor</source>
        <translation type="vanished">Fonte padrão do editor de notas</translation>
    </message>
    <message>
        <source>Default &amp;name</source>
        <translation type="vanished">&amp;Nome padrão</translation>
    </message>
    <message>
        <source>&amp;Content font</source>
        <translation type="vanished">&amp;Fonte do conteúdo</translation>
    </message>
    <message>
        <source>&amp;Database file:</source>
        <translation>&amp;Arquivo do banco de dados:</translation>
    </message>
    <message>
        <source>Default &amp;name:</source>
        <translation>Nome &amp;padrão:</translation>
    </message>
    <message>
        <source>Command for calling the spell checker</source>
        <translation>Comando para chamar o verificador ortográfico</translation>
    </message>
    <message>
        <source>&amp;Spell command:</source>
        <translation>Comando do &amp;verificardor ortográfico:</translation>
    </message>
    <message>
        <source>Default font of the note editor (content)</source>
        <translation>Fonte padrão para o editor de notas (conteúdo)</translation>
    </message>
    <message>
        <source>&amp;Content font:</source>
        <translation>&amp;Fonte do conteúdo:</translation>
    </message>
    <message>
        <source>Command to get the available dictionaries</source>
        <translation>Comando para listar os dicionários disponíveis</translation>
    </message>
    <message>
        <source>List dictionaries:</source>
        <translation>Listar dicionários:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Untitled</source>
        <translation>Sem nome</translation>
    </message>
    <message>
        <source>QJournal is already running.</source>
        <translation>QJournal já está sendo executado.</translation>
    </message>
</context>
<context>
    <name>SpellDialog</name>
    <message>
        <source>Check word spelling</source>
        <translation>Verificar grafia da palavra</translation>
    </message>
    <message>
        <source>Unknown word:</source>
        <translation>Palavra desconhecida:</translation>
    </message>
    <message>
        <source>Replace &amp;with:</source>
        <translation>Substituir &amp;com:</translation>
    </message>
    <message>
        <source>&amp;Language:</source>
        <translation>&amp;Linguagem:</translation>
    </message>
    <message>
        <source>&amp;Replace</source>
        <translation>&amp;Substituir</translation>
    </message>
    <message>
        <source>Replace &amp;all</source>
        <translation>SUbstituir &amp;todos</translation>
    </message>
    <message>
        <source>&amp;Ignore</source>
        <translation>&amp;Ignorar</translation>
    </message>
    <message>
        <source>I&amp;gnore all</source>
        <translation>I&amp;gnorar todos</translation>
    </message>
    <message>
        <source>Spell check done.</source>
        <translation>Verificação ortográfica concluída.</translation>
    </message>
    <message>
        <source>Spell check complete.</source>
        <translation>Verificação ortográfica concluída.</translation>
    </message>
</context>
</TS>
