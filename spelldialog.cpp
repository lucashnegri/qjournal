#include <QClipboard>
#include <QStringList>
#include <QLocale>
#include <QMessageBox>
#include <QTextEdit>

#include "spelldialog.h"
#include "ui_spelldialog.h"

SpellDialog::SpellDialog(QWidget *parent)
    : QDialog(parent), spell(new SpellBackend()), ui(new Ui::SpellDialog())
{
    ui->setupUi(this);
}

SpellDialog::~SpellDialog()
{
    delete ui;
}

void SpellDialog::exec(QTextEdit *edit, const QString &spellCommand, const QString &spellDicts)
{
    spell->setCommands(spellCommand, spellDicts);
    spell->setLanguage(QLocale::system().name());
    this->edit = edit;

    // populate the combo box with the available languages
    bool ok;
    ui->comboLanguage->clear();
    ui->comboLanguage->insertItem(0, QLocale::system().name());
    ui->comboLanguage->insertItems(1, spell->getAvailableLanguages(ok));

    beginCheck();
    QDialog::exec();
}

void SpellDialog::beginCheck()
{
    // enable controls
    ui->btnIgnore->setEnabled(true);
    ui->btnIgnoreAll->setEnabled(true);
    ui->btnReplace->setEnabled(true);
    ui->btnReplaceAll->setEnabled(true);

    started = true;

    // clear ignored and replace all words
    ignoredWords.clear();
    replaceAllWords.clear();

    // initiate the correction loop
    correctLoop();
}

bool SpellDialog::moveNext()
{
    if(started)
    {
        // move to the begining
        QTextCursor cursor = edit->textCursor();
        cursor.setPosition(0, QTextCursor::MoveAnchor);
        edit->setTextCursor(cursor);
        started = false;
        return false;
    }
    else
    {
        // move to the next word
        QTextCursor cursor = edit->textCursor();
        bool hitEnd = !cursor.movePosition(QTextCursor::NextWord, QTextCursor::MoveAnchor);
        edit->setTextCursor(cursor);
        return hitEnd;
    }
}

void SpellDialog::checkDone()
{
    ui->btnIgnore->setEnabled(false);
    ui->btnIgnoreAll->setEnabled(false);
    ui->btnReplace->setEnabled(false);
    ui->btnReplaceAll->setEnabled(false);
    QMessageBox::information(this, tr("Spell check done."), tr("Spell check complete."));
}

void SpellDialog::correctLoop()
{
    bool hitEnd = false;

    while(true)
    {
        QCoreApplication::processEvents();

        hitEnd = moveNext();

        if(hitEnd)
            break;

        if(!correctCurrent())
            break;
    }

    if(hitEnd)
        checkDone();
}

bool SpellDialog::correctCurrent()
{
    ui->labelWord->clear();
    ui->lineReplace->clear();
    ui->listSuggestions->clear();

    /* get the current word, or return false if it does not exists */
    QTextCursor cursor = edit->textCursor();
    cursor.movePosition(QTextCursor::EndOfWord, QTextCursor::KeepAnchor);
    edit->setTextCursor(cursor);
    QString word = cursor.selectedText();

    /* skip empty words */
    if(word.isEmpty())
        return true;

    /* get the word and show the available corrections */
    QStringList list;

    bool ok;
    bool correct = spell->correct(word, list, ok);

    if(!ok || correct)
        return true;

    /* if the user selected replace all, we replace it right here */
    if(replaceAllWords.contains(word))
    {
        ui->lineReplace->setText(replaceAllWords[word]);
        replace();
        return true;
    }

    /* if the user selected to ignore all, just ignore */
    if(ignoredWords.contains(word))
        return true;

    /* incorrect word. show a list of suggestions */
    ui->labelWord->setText(word);
    ui->listSuggestions->clear();
    ui->listSuggestions->addItems(list);

    return false;
}

void SpellDialog::replace()
{
    QString correction = ui->lineReplace->text();
    QTextCursor cursor = edit->textCursor();
    cursor.insertText(correction);
    correctLoop();
}

void SpellDialog::replaceAll()
{
    QString oldString = ui->labelWord->text();
    QString newString = ui->lineReplace->text();
    replaceAllWords[oldString] = newString;
    replace();
}

void SpellDialog::ignore()
{
    correctLoop();
}

void SpellDialog::ignoreAll()
{
    QString oldString = ui->labelWord->text();
    ignoredWords[oldString] = true;
    ignore();
}

void SpellDialog::suggestionClicked(QListWidgetItem *item)
{
    ui->lineReplace->setText(item->text());
}

void SpellDialog::languageChanged(QString language)
{
    spell->setLanguage(language);
    beginCheck();
}
