#include <QTranslator>
#include <QLocale>
#include <QApplication>
#include <QIcon>
#include <QSharedMemory>
#include <QMessageBox>

#include "config.h"
#include "noteeditor.h"
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    // translations
    QString locale = QLocale::system().name();
    QString trans_file = QString("%1/qjournal_%2.qm").arg(TRANSLATIONS_PATH, locale);
    QTranslator translator;
    translator.load(trans_file);

    // application
    QApplication a(argc, argv);
    a.setOrganizationName("OProj");
    a.setApplicationName("QJournal");
    a.installTranslator(&translator);

    // only one instance
    QSharedMemory mem("QJournalMemKey4283623");

    if(!mem.create(1))
    {
        if(mem.error() == QSharedMemory::AlreadyExists)
            QMessageBox::information(nullptr, "QJournal", QObject::tr("QJournal is already running."));
        else
            QMessageBox::warning(nullptr, "QJournal", mem.errorString());

        return 0;
    }

// icon theme
#ifndef Q_OS_UNIX
    QIcon::setThemeName(ICON_FALLBACK);
#endif

    // run
    MainWindow w;
    w.run();

    return a.exec();
}
