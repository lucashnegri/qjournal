#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QObject>
#include <QtSql>

class QSqlTableModel;
class QStringList;

/**
 * @brief Database manager. All database operations are performed here.
 */
class Database : public QObject
{
    Q_OBJECT
public:
    explicit Database(QObject *parent = nullptr);
    bool loadOrCreate(const QString &name);

    // Caller must manage the returned models
    QSqlTableModel *makeCategoriesModel();
    QSqlTableModel *makeNotesModel(int category_id);
    QSqlTableModel *makeNotesModel(const QString &date);
    QSqlTableModel *makeSearchModel(const QString &from, const QString &to, const QString &name,
                                    const QString &category, const QString &contents);

    // Caller must manage the returned QStringList*
    QStringList *daysWithNotesInTheMonth(const QDate &date);

    // CRUD (editing is handled by the model)
    void noteAdd(int category_id, const QString &name, const QDate &date, const QString &content="");
    void noteDelete(int note_id);
    void categoryAdd(const QString &name);
    void categoryDelete(int category_id);

    void notesStatistics(const QDate &date, int &total, int &year, int &month);

private:
    Q_DISABLE_COPY(Database)

    int notesHelper(const QString &sql);
    QSqlTableModel *makeNotesModel();

    QSqlDatabase db;
};

#endif // DATABASEMANAGER_H
